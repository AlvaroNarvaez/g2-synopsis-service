package p.com.synopsis.beans.response;

import java.util.List;

import p.com.synopsis.beans.Usuario;

public class ListaUsuarioResponse {
	
	public List<Usuario> listaUsuario;

	public List<Usuario> getListaUsuario() {
		return listaUsuario;
	}

	public void setListaUsuario(List<Usuario> listaUsuario) {
		this.listaUsuario = listaUsuario;
	}
}
