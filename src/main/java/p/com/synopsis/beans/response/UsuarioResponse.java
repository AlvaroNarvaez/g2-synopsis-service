package p.com.synopsis.beans.response;

import p.com.synopsis.beans.Usuario;

public class UsuarioResponse {
	
	private Usuario usuario;

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}
}
