package p.com.synopsis.service.impl;

import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import p.com.synopsis.beans.Usuario;
import p.com.synopsis.beans.response.ListaUsuarioResponse;
import p.com.synopsis.beans.response.UsuarioResponse;
import p.com.synopsis.dao.UsuarioDao;
import p.com.synopsis.service.UsuarioService;

@Service
public class UsuarioServiceImpl implements UsuarioService {
	
	@Autowired
	UsuarioDao usuarioDao;
	
	@Override
	public UsuarioResponse obtenerDatosUsuario(int codigo) {
		UsuarioResponse response = new UsuarioResponse();
		
		response.setUsuario(usuarioDao.obtenerUsuario(codigo));
		
		return response;
	}

	@Override
	public ListaUsuarioResponse listaUsuarios() {
		ListaUsuarioResponse response = new ListaUsuarioResponse();
		List<Usuario> listaUsuario = usuarioDao.listaUsuarios();
		
		response.setListaUsuario(listaUsuario);
		return response;
	}

	@Override
	public Integer insertarUsuario(Usuario usuario) {
		int response = usuarioDao.insertarUsuario(usuario);
		return response;
	}

	@Override
	public Integer actualizarUsuario(Usuario usuario) {
		int response = usuarioDao.actualizarUsuario(usuario);
		return response;
	}

	@Override
	public Integer eliminarUsuario(int codigo) {
		int response = usuarioDao.eliminarUsuario(codigo);
		return response;
	}
}
