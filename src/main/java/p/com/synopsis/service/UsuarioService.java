package p.com.synopsis.service;

import java.sql.SQLException;

import p.com.synopsis.beans.Usuario;
import p.com.synopsis.beans.response.ListaUsuarioResponse;
import p.com.synopsis.beans.response.UsuarioResponse;

public interface UsuarioService {
	
	public UsuarioResponse obtenerDatosUsuario(int codigo);
	
	public ListaUsuarioResponse listaUsuarios();
	
	public Integer insertarUsuario(Usuario usuario);
	
	public Integer actualizarUsuario(Usuario usuario);
	
	public Integer eliminarUsuario(int codigo);
}
