package p.com.synopsis.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.support.JdbcDaoSupport;
import org.springframework.stereotype.Repository;

import p.com.synopsis.beans.Usuario;
import p.com.synopsis.dao.UsuarioDao;

@Repository
public class UsuarioDaoImpl extends JdbcDaoSupport implements UsuarioDao{
	
	@Autowired
	DataSource datasource;
	
	@PostConstruct
	private void initialize() {
		setDataSource(datasource);
	}
	
	@Override
	public Usuario obtenerUsuario(int codigo) {
		Usuario usuario = new Usuario();
		String sql = "SELECT * FROM USUARIO WHERE CODIGO=?";
		
		try {
			usuario = getJdbcTemplate().queryForObject(sql, new Object[] {codigo}, new RowMapper<Usuario>() {
				
				@Override
				public Usuario mapRow(ResultSet rs, int rowNum) throws SQLException {
					Usuario resultado = new Usuario();
					resultado.setCodigo(rs.getInt("codigo"));
					resultado.setNombre(rs.getString("nombre"));
					resultado.setApellido(rs.getString("apellido"));
					resultado.setEdad(rs.getInt("edad"));
					return resultado;
				}
			});
		} catch (Exception e) {
			
		}
		
		return usuario;
	}

	@Override
	public List<Usuario> listaUsuarios() {
		String sql = "SELECT * FROM USUARIO";
		List<Usuario> listaUsuarios = new ArrayList<Usuario>();
		List<Map<String, Object>> resultados = getJdbcTemplate().queryForList(sql);
		
		for(Map<String, Object> resultado : resultados) {
			Usuario usuario = new Usuario();
			usuario.setCodigo((int) resultado.get("codigo"));
			usuario.setNombre((String) resultado.get("nombre"));
			usuario.setApellido((String) resultado.get("apellido"));
			usuario.setEdad((int) resultado.get("edad"));
			listaUsuarios.add(usuario);
		}
		return listaUsuarios;
	}

	@Override
	public Integer insertarUsuario(Usuario usuario) {
		String sql = "INSERT INTO usuario (codigo, nombre, apellido, edad) VALUES (?, ?, ?, ?)";
		int respuesta = getJdbcTemplate().update(sql, new Object[] {listaUsuarios().size()+1, usuario.getNombre(), usuario.getApellido(), usuario.getEdad()});
		return respuesta;
	}

	@Override
	public Integer actualizarUsuario(Usuario usuario) {
		String sql = "UPDATE usuario SET nombre = ?, apellido = ?, edad = ? WHERE codigo = ?";
		int respuesta = getJdbcTemplate().update(sql, new Object[] {usuario.getNombre(), usuario.getApellido(), usuario.getEdad(), usuario.getCodigo()});
		return respuesta;
	}

	@Override
	public Integer eliminarUsuario(int codigo) {
		String sql = "DELETE FROM usuario WHERE codigo = ?";
		int respuesta = getJdbcTemplate().update(sql, new Object[] {codigo});
		return respuesta;
	}
	
}
