package p.com.synopsis.dao;

import java.util.List;

import p.com.synopsis.beans.Usuario;

public interface UsuarioDao {
	
	public Usuario obtenerUsuario(int codigo);
	
	
	public List<Usuario> listaUsuarios();
	
	public Integer insertarUsuario(Usuario usuario);
	
	public Integer actualizarUsuario(Usuario usuario);
	
	public Integer eliminarUsuario(int codigo);
}
